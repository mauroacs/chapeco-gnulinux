#!/bin/bash

#VERIFICA A EXISTENCIA DO PACOTE X11VNC
dependencia=`dpkg -l x11vnc | grep x11vnc | awk -v FS= '{print $1$2}'`

instalar(){
sudo apt-get install x11vnc zenity
sudo mkdir /opt/prefeitura/saude/
sudo chown $USER:$USER -R /opt/prefeitura/saude/
sudo mv startvnc.sh /opt/saude
sudo ln -s /opt/prefeitura/saude/startvnc.sh -O /usr/bin/saudevnc
sudo wget http://suportesaude2.chapeco.sc.gov.br/Downloads/scripts/Suporte.desktop -O /usr/share/applications/Suporte.desktop

}

if [ $dependencia != "ii" ]; then
   instalar
fi


on(){
    pkill x11vnc
    x11vnc -noxdamage  -display :0 -passwd info2000 -wait 50 -forever -bg
    ip=`hostname -I`

    echo >> /opt/prefeitura/saude/ip_historico.txt
    echo "======================================" >> /opt/prefeitura/saude/ip_historico.txt
    date >> /opt/prefeitura/saude/ip_historico.txt
    hostname -I >>/opt/prefeitura/saude/ip_historico.txt
    echo "=====================================" >> /opt/prefeitura/saude/ip_historico.txt
    echo >> /opt/prefeitura/saude/ip_historico.txt

    pc=`hostname`
    zenity --title="Suporte Tecnico Remoto" --width=80 heigth=120 --info --text="Suporte Técnico Remoto está Habilitado.\n\nComputador = $pc\nIP = $ip\nSuporte Ligue: 3321-8532"
}

off()
    {
    pkill x11vnc
    x11vnc -noxdamage  -display :0 -passwd info2000 -wait 50 -forever -bg
}

msg() {
    zenity --title="Suporte Técnico Remoto" --info --text="Favor Configurar as opções"
}

case $1 in

 "-on") on  ;;
"-off") off ;;
"install") instalar;;
     *) msg ;;
esac
