#!/bin/bash
#Script de preparo do sistema pós-instalação

function msg(){
echo -e '\033[01;32m '$1' \033[00;37m'
}

platforma=`uname --machine`
echo ===============================================
echo  EXECUTE SOMENTE APÓS A INSTALAÇÃO DO SISTEMA!
echo ===============================================
echo
msg "Deseja fazer os ajuster da Imagem Agora? [N/y]"
read resposta
if [ $resposta == "y" ] || [ $resposta == "Y" ]; then
	inicio=`pwd`
	sudo apt update
	sudo apt upgrade -y
	sudo apt remove -y  cups-browsed calamares cryptsetup cryptsetup-bin && sudo apt autoremove -y
	sudo rm /home/usuario/Desktop/install-debian.desktop
	sudo apt install openssh-server -y
	sudo mv /opt/prefeitura/sshd_config /etc/ssh/sshd_config
	sudo rm $inicio/Desktop/calamares.desktop
	sudo rm /home/$USER/.config/autostart/autorun.desktop
	sudo chown $USER:$USER /etc/sddm.conf
	echo "[Autologin]" > /etc/sddm.conf
	echo "User="`echo $USER` >> /etc/sddm.conf
	echo "Session=lxqt.desktop" >> /etc/sddm.conf
	echo "Relogin=true" >> /etc/sddm.conf
	echo
	if [ $plataforma == "x86_64" ]; then
		sudo cp /usr/share/applications/google-chrome.desktop /home/$USER/Desktop/google-chrome.desktop
		else
		sudo cp /usr/share/applications/chromium-browser.desktop /home/$USER/Desktop/chromium-browser.desktop
	fi
	msg "Esta imagem é para [S]aúde ou [E]ducação? [S/E]"
	read tipoimagem

	if [ $tipoimagem == "S" ] || [ $tipoimagem == "s" ]; then
		sudo ln -s /opt/prefeitura/saude/meuip.sh	/usr/bin/suporte
		sudo ln -s /opt/prefeitura/saude/startvnc.sh	/usr/bin/saudevnc
		sudo ln -s /opt/prefeitura/saude/configura.sh	/usr/bin/linuxsaude
	fi

	if [ $tipoimagem == "E" ] || [ $tipoimagem == "e" ]; then
		sudo mv /home/$USER/Desktop/idsweb.desktop 	/opt/prefeitura/saude/
		sudo mv /usr/share/applications/idsweb.desktop	/opt/prefeitura/saude/
		sudo cp /usr/share/applications/firefox-esr.desktop /home/$USER/Desktop/firefox-esr.desktop
		sudo ln -s /opt/prefeitura/educacao/startvnc.sh /usr/bin/saudevnc

	fi
	sudo rm -rf /home/public
	sudo apt install -y samba smbclient smbldap-tools smb4k
	sudo chown $USER:$USER /etc/samba/smb.conf
	sudo echo ""  >> /etc/samba/smb.conf
	sudo echo "[Public]" >> /etc/samba/smb.conf
	sudo echo "   comment = Documentos Compartilhados" >> /etc/samba/smb.conf
	sudo echo "   browseable = yes" >> /etc/samba/smb.conf
	sudo echo "   path = /home/public" >> /etc/samba/smb.conf
	sudo echo "   printable = no" >> /etc/samba/smb.conf
	sudo echo "   guest ok = yes" >> /etc/samba/smb.conf
	sudo echo "   read only = no" >> /etc/samba/smb.conf
	sudo echo "   create mask = 0777" >> /etc/samba/smb.conf
	sudo echo ""  >> /etc/samba/smb.conf
	sudo adduser public --disabled-login
	sudo adduser $USER public
	sudo chmod 0777 -R /home/public
	sudo rm -rf /home/$USER/Público
	sudo ln -s /home/public /home/$USER/Público
	sudo ln -s /home/public /home/$USER/Desktop/Scanner
	sudo /etc/init.d/smbd stop
	sudo /etc/init.d/smbd start
	sudo rm -rf /home/public/*
	sudo rm -rf /home/public/.*
	sudo update-grub2
	sudo init 6
	else
	msg "Até a Proxima!!"
	read
	exit 0
fi

