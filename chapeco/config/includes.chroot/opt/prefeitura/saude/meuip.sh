#!/bin/bash

#Informa o IP do computador
echo "IP do Computador" > /opt/prefeitura/saude/ip.txt
hostname -I >>/opt/prefeitura/saude/ip.txt
echo  >>/opt/prefeitura/saude/ip.txt

#Mostra o Nome do Computador
echo "Nome do Computador" >> /opt/prefeitura/saude/ip.txt
hostname >>/opt/prefeitura/saude/ip.txt
echo >>/opt/prefeitura/saude/ip.txt

#Mostra o usuario logado no sistema
echo "Usuário de Logado no Linux" >>/opt/prefeitura/saude/ip.txt
whoami >>/opt/prefeitura/saude/ip.txt
echo >>/opt/prefeitura/saude/ip.txt
#Mostra nome do Usuaro do Winsaude
echo "Usuário de Acesso ao Winsaude" >>/opt/prefeitura/saude/ip.txt

if (  cat /opt/prefeitura/saude/saude.sh | grep "defaultUser" ) then
cat /opt/prefeitura/saude/saude.sh | grep "defaultUser=" | awk -F "=" '{print substr($2,1,15)}' >>/opt/prefeitura/saude/ip.txt
else 
cat /opt/prefeitura/saude/saude.sh | grep "prefeitura=" | awk -F "-u" '{print substr($2,1,30)}' | awk -F " " '{print $1}'>>/opt/prefeitura/saude/ip.txt
fi

echo >>/opt/prefeitura/saude/ip.txt
#Mostra a versao do Sistema Operacional - Release
echo "Versão do Sistema Operacional" >>/opt/prefeitura/saude/ip.txt
lsb_release -ds >>/opt/prefeitura/saude/ip.txt
echo "Versão do Kernel " >> /opt/prefeitura/saude/ip.txt
uname -r >>/opt/prefeitura/saude/ip.txt

zenity --text-info --title="Suporte Técnico" --filename=/opt/prefeitura/saude/ip.txt
