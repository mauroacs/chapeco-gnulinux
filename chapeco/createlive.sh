#!/bin/bash
# REFERENCIAS:
# https://manpages.debian.org/stretch/live-build/lb_config.1.fr.html
# https://live-team.pages.debian.net/live-manual/html/live-manual/customizing-package-installation.en.html
# https://wiki.ubuntu.com/S390X/InstallationGuide/AutomatedInstallsWithPreseed?action=AttachFile&do=get&target=preseed_kvm.cfg
# https://www.vivaolinux.com.br/artigo/Criando-um-LiveCD-a-partir-de-uma-instalacao-do-Debian-Lenny
# https://framagit.org/dflinux/DFiso/
#
# Desenvolvido para DuZeru 4 Edição 0 DZ 4.0
# OBS: Debian 10 utiliza o padrão su - para acessar como root
# Autor: Cláudio A. Silva
# git clone https://gitlab.com/duzeru/live-build.git

#  0) versao='bookworm && sudo cp branding/bookworm config/includes.chroot/etc/calamares/branding/debian/branding.desc;;


#FUNÇÃO PARA EXIBIR MENSAGENS AO USUÁRIO DO SCRIPT
function msg(){
echo -e '\033[01;32m '$1' \033[00;37m'
}
#VERIFICA SE INICIOU O SCRIPT COMO ROOT
if [ `whoami` != root ]; then
    msg "erro : Execute createlive.sh como root"
    exit 1
fi

inicio=`date`
ARCH=$1
versao=$2

if   [ $versao == "bookworm" ]; then sudo cp branding/bookworm config/includes.chroot/etc/calamares/branding/debian/branding.desc
elif [ $versao == "bullseye" ]; then sudo cp branding/bullseye config/includes.chroot/etc/calamares/branding/debian/branding.desc
elif [ $versao == "buster"   ]; then sudo cp branding/buster   config/includes.chroot/etc/calamares/branding/debian/branding.desc 
fi

if   [ $ARCH == "32"    ]; then ARCH="i386"
elif [ $ARCH == "64"    ]; then ARCH="amd64"
else
 	# Se nenhum argumento for passado exibe a Ajuda
    	clear
    	msg  "Script para Geração da Imagem Live Debian para Prefeitura de Chapecó"
    	echo "======================================================================"
    	echo " Somente em  modo Administrador - Modo de Utilização"
    	echo
    	echo " bash createlive.sh 32    > Constroi o sistema em modo i386  - 32 bits"
    	echo " bash createlive.sh 64    > Constroi o sistema em modo amd64 - 64 bits "
    	echo
    exit 1
fi

#INSTALA AS DEPENDENCIAS NECESSÁRIAS
msg "Instalando dependencias necessárias à geração da Imagem:"
sleep 2
apt-get install git live-build live-manual live-config schroot -y
# CRIAR O DIRETORIO NECESSARIO PARA GERACAO DA LIVE
home=`pwd`
live="$home/../$versao-$ARCH"
rm -rf $live
mkdir $live
cd $live

lb config -a ${ARCH} \
--mode debian \
--distribution $versao \
--parent-distribution $versao \
--debian-installer-distribution $versao \
--apt-indices true \
--backports false \
--image-name Chapeco \
--debian-installer-gui true \
--debian-installer live \
--archive-areas "main contrib non-free" \
--parent-archive-areas "main contrib non-free" \
--bootappend-live "boot=live components hostname=prefeitura username=usuario" \
--updates false \
--memtest memtest86+ \
--uefi-secure-boot auto \
--iso-publisher "Mauro A C Siqueira <mauroacsiqueira@gmail.com>" \

msg "Copiando arquivos em $live ..."

sleep 2
cp -r $home/config/* $live/config/

if [ $ARCH == "i386" ]; then
	cp -r $home/config32/* 	  	$live/config/
#	cp $home/postInstall32.sh 	$live/config/includes.chroot/opt/prefeitura/postInstall.sh
	cp $home/autorun.desktop  	$live/config/includes.chroot/etc/skel/.config/autostart/autorun.desktop
fi
if [ $ARCH == "amd64" ]; then
	cp -r $home/config64/*    	$live/config/
#	cp $home/postInstall64.sh 	$live/config/includes.chroot/opt/prefeitura/postInstall.sh
	cp $home/autorun.desktop  	$live/config/includes.chroot/etc/skel/.config/autostart/autorun.desktop
fi

msg 'Arquivos copiados com sucesso'
sleep 2

lb build
fim=`date`
echo
echo "O sistema iniciou a construção em: "$inicio
echo "E terminou às: "$fim
echo
